// omok.h

#ifndef _OMOK_H_
#define _OMOK_H_

#include <iostream>
#include <map>

using namespace std;

class Omok {
 public:
  Omok():IsBlacksTurn_(false){};
  bool Put(int x, int y);
  bool IsOmok(bool* is_winner_black) const;
  bool IsBlacksTurn() const{return IsBlacksTurn_;};
  map<pair<int, int>,bool>& data(){return data_;}

 private:  // Define your private members here.
  bool IsBlacksTurn_;
  map<pair<int, int>, bool> data_;
//  pair<int,int> last_;//연경이 방식대로 가자.//일단 끝내고 말야.	
};

ostream& operator<<(std::ostream& os, const Omok& omok);

#endif  // _OMOK_H_
