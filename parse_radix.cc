//parse_radix.cc
#ifndef _PARSE_RADIX_CC_
#define _PARSE_RADIX_CC_

#include "parse_radix.h"
#include <vector>
/*
string CUMULATED_ERROR_MESSAGE
void SHOW_ERROR(){
	cout<<"\t\t"<<CUMULATED_ERROR_MESSAGE<<endl;
	CUMULATED_ERROR_MESSAGE.clear();
}
*/

using namespace std;

int ctoi(char c){
  if('0'<=c && c<='9') return c-'0';
  if('a'<=c && c<='z') return c-'a'+10;
  return 0;
}
char itoc(int i){
  if(0<=i && i<=9) return i+'0';
  if(10<=i && i<='z'-'a'+10) return i+'a'-10;
  return 'X';
}



Number operator+(const Number& a, const Number& b){return Number(a.num+b.num);}
Number operator-(const Number& a, const Number& b){return Number(a.num-b.num);}
Number operator*(const Number& a, const Number& b){return Number(a.num*b.num);}
Number operator/(const Number& a, const Number& b){return Number(a.num/b.num);}

bool operator< (const Number& a, const Number& b){return a.num<b.num;}
bool operator> (const Number& a, const Number& b){return a.num>b.num;}
bool operator>=(const Number& a, const Number& b){return a.num<=b.num;}
bool operator<=(const Number& a, const Number& b){return a.num>=b.num;}
bool operator==(const Number& a, const Number& b){return a.num==b.num;}

Number&operator%(Number& a,const int& n){a.rad=n; return a;}

std::istream& operator>>(std::istream& is, Number& a){
  char c='0';
  int rad=10, num=0;
  vector<int>nums;
  while(true){
    is>>c;
	if(c=='_' || c==' ') break;
    nums.push_back(ctoi(c));
  }

  if(nums.size()!=1)
    is>>rad;
  a.rad=rad;
  int i=0;
  while(true){
    num+=nums[i];
	i++;
	if(i<nums.size()){
		num*=rad;	
	}
	else	break;
  }
  a.num=num;
  return is;
}

std::ostream& operator<<(std::ostream& os, const Number& a){
  int n=a.num,d=1;
  for(;n/(d*a.rad)!=0; d*=a.rad)
	  continue;

  while(d!=0){
    os<<itoc(n/d);
    n%=d;
	d/=a.rad;
  }
  os<<'_'<<a.rad;
 return os;
}

#endif
