// omok_main.cc

#include <iostream>
#include "omok.h"
  
using namespace std;
  
int main() {
  Omok omok;
   while (true) {
    int x, y;
    cout << (omok.IsBlacksTurn() ? "Black: " : "White: ");
    cin >> x >> y;
   if ((cin.rdstate() & istream::failbit) != 0) break;
    if (omok.Put(x, y) == false) {
      cout << "Can not be placed there." << endl;
      // cout << omok;  // Uncomment this line for debugging.
      continue;
    } else {
      bool is_winner_black;
      // cout << omok;
      if (omok.IsOmok(&is_winner_black)) {
        cout << omok;
        cout << (is_winner_black ? "Winner: Black player" :
                                   "Winner: White player") << endl;
        break;
      }
    }
  }
  return 0;
}                

