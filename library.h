// library.h

#ifndef _LIBRARY_H_
#define _LIBRARY_H_

#include <iostream>
#include <vector>

class Book {
 public:
	 Book(){}
	 Book(const Book& book):title_(book.title_), total_(book.total_), lend_(0),not_magazine(false){}
	 Book(const std::string& title, int total=1):title_(title), total_(total), lend_(0),not_magazine(false){}

	 std::string title() const{return title_;};	 // 도서의 제목
	 int total() const{return total_;}	 // 해당 도서의 총 보유 권수
	 int stock() const{return total_-lend_;}	 // 도서의 재고 (남아있는 권수)

	 bool Lend(int count = 1){if(lend_<total_){lend_+=1; return true; }else{ return false;};}
	 bool Return(int count = 1){if(lend_!=0  ){lend_-=1; return true; }else{ return false;};}
	 bool More(int count = 1){total_+=1;}
	 bool not_magazine;
 protected:
	 std::string title_;
	 int total_,lend_;
	 // 멤버변수를 정의.
};

std::string str(int i);

class Magazine : public Book {
 public:
	 Magazine(){}
	 Magazine(const Magazine& magazine){title_=(magazine.title_), total_=(magazine.total_), lend_=(magazine.lend_),not_magazine=(false);
	 }
	 Magazine(const std::string& title, int year, int month,int total) {
		 title_=title, total_=total, lend_=0;
		 title_+=("("+str(year)+"/"+str(month)+")");
	 }// 잡지의 제목 'PCMagazine(13/9)' 처럼 년/월을 원래 제목에 추가하여 리턴.
	 //std::string title() const;

 protected:
	 // 멤버변수를 정의. 싹다 계승했어 ㄱㅊㄱㅊ
};

// Input을 처리하는 operator 구현.
std::ostream& operator<<(std::ostream& os, Book& book);
//std::istream& operator>>(std::istream& is, Magazine& magazine);


class Library {
 public:
	 Library(){}	 // 디폴트 생성자, 카운트를 0으로 셋팅
	 ~Library(){}	 // 소멸자, 메모리 해제
	 
	 void AddBook(const std::string& title, int num);// 도서 정보를 추가하는 함수
	 void AddMagazine(const std::string& title,int year, int month, int num);
	 bool DeleteBook(const std::string& title);// 도서 정보를 삭제하는 함수

	 bool Lend(const std::string& title); // 책 한권을 대여하는 함수
	 bool Return(const std::string& title); // 책 한권을 반납하는 함수
	 void PrintAll(); // 현재 저장되어 있는 도서(책과 잡지)의 리스트를 출력.
	 	 	 	 	 	 	 	 	  // 출력형식: stock/total title
private:
	 std::vector<Book> books_;	 // 도서를 저장하는 배열
	 std::vector<Magazine> magazines_;	 // 잡지를 저장하는 배열
};

#endif	 // _LIBRARY_H_
