#include "library.h"
#include <iostream>

using namespace std;

int main(){
	string command, title;
	int year, month, num;
	Library l;
	while(command!="quit"){
		cin>>command;
		
		if(command== "add_book"){
			cin>>title>>num;
			l.AddBook(title,num);
		}else if(command== "add_magazine"){
			cin>>title>>year>>month>>num;
			l.AddMagazine(title,year,month,num);
		}else if(command== "delete"){
			cin>>title;
			l.DeleteBook(title);
		}else if(command== "lend"){
			cin>>title;
			if(l.Lend(title)==false)
				cout<<"out of stock."<<endl;
		}else if(command== "return"){
			cin>>title;
			if(l.Return(title)==false)
				cout<<"invalid operation."<<endl;
		}else if(command== "print"){
			l.PrintAll();
		}
	}
}
