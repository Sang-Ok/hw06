#include "point2d.h"

Point operator-(const Point& one){return Point(-one.x,-one.y);}
Point operator+(const Point& l, const Point& r){return Point(l.x+r.x, l.y+r.y);}
Point operator-(const Point& l, const Point& r){return Point(l.x-r.x, l.y-r.y);}
Point operator*(const Point& l, const Point& r){return Point(l.x*r.x, l.y*r.y);}
Point operator+=(Point& l, const Point& r){return (l=l+r);}
Point operator-=(Point& l, const Point& r){return (l=l-r);}
Point operator*=(Point& l, const Point& r){return (l=l*r);}

istream& operator>>(istream& is, Point& pt){
  int x,y;
  is>>x>>y;
  pt=Point(x,y);
  return is;
}
ostream& operator<<(ostream& os, const Point& pt){
  os<<'('<<pt.x<<", "<<pt.y<<')';
  return os;
}
