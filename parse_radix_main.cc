//parse_radix_main.cc
#include "parse_radix.h"
#include <string>
#include <iostream>
using namespace std;

void DoOperate(Number a, string op, Number b){
  int rad;
  if(op =="<"){		cout<<(a< b? "true":"false")<<endl;	return;
  }else if(op =="<="){	cout<<(a<=b? "true":"false")<<endl;	return;
  }else if(op ==">") {	cout<<(a> b? "true":"false")<<endl;	return;
  }else if(op ==">="){	cout<<(a>=b? "true":"false")<<endl;	return;
  }else if(op =="=="){	cout<<(a==b? "true":"false")<<endl;	return;
  }else{
	  cin>>rad;
  	  if(op=="+"){		Number ret(a+b);	cout<<ret%rad<<endl;	return;
	  }else if(op=="-"){	Number ret(a-b);	cout<<ret%rad<<endl;	return;
	  }else if(op=="*"){	Number ret(a*b);	cout<<ret%rad<<endl;	return;
	  }else if(op=="/"){	Number ret(a/b);	cout<<ret%rad<<endl;	return;//왜 ret을 만들어야하지?
	  }
  }
  cout<<"Error"<<endl;
}


int main(){
  string command, op;
  Number a,b;
  int rad;
  while(true){
    cin>>command;
    if(command=="quit"){ break;
    }else if(command=="eval"){
      cin>>a>>op>>b;
        DoOperate(a,op,b);
    }else{cout<<"Error"<<endl;}
  }
}

