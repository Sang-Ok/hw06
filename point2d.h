// point2d.h

#ifndef _POINT2D_H_
#define _POINT2D_H_

#include <iostream>
using namespace std;

class Point {
public:
//  Point();					//이건 필요없어보이는데...
  explicit Point(int c=0):x(c), y(c){}
  Point(int a, int b) : x(a), y(b){}		//Point(int a=0, int b=a)같은거 되면 한번에 3개 선언하는 셈인데... 쩝..
  Point(const Point& p) :x(p.x), y(p.y){}

  int x, y;
						//'='연산자는 자동입니까?
  Point operator-() const;	// 전위 - 연산자
  Point& operator+=(const Point& p);
  Point& operator-=(const Point& p);
  Point& operator*=(const Point& p);
};

Point operator+(const Point& lhs, const Point& rhs);
Point operator-(const Point& lhs, const Point& rhs);
Point operator*(const Point& lhs, const Point& rhs);

std::istream& operator>>(std::istream& is, Point& pt);
std::ostream& operator<<(std::ostream& os, const Point& pt);

#endif  // _POINT2D_H_
