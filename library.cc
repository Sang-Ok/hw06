#include "library.h"
#include <iostream>
#define COUT	cout<<"\t\ttest::"
using namespace std;
bool operator==(string a, string b){	return (a.compare(b))==0;}
bool operator!=(string a, string b){	return !(a==b);}
ostream& operator<<(ostream& os, Book& book){	os<<book.stock()<<'/'<<book.total()<<' '<<book.title(); return os;}

void Library::AddBook(const string& title, int num){
	int i=0;				COUT<<"start AddBook("<<title<<","<<num<<")"<<endl;
	for(i=0; i<books_.size(); i++){
		if(title.compare(books_[i].title()) ==0)
			break;
	}
	if(i<books_.size() ){
		books_[i].More();
	}else{
		books_.push_back(Book(title,num));
	}
}
void Library::AddMagazine(const string& title,int year, int month, int num){
	int i=0;			COUT<<"start AddMagazine("<<title<<","<<year<<","<<month<<","<<num<<")"<<endl;
	for(i=0; i<magazines_.size(); i++){
		if(title.compare(magazines_[i].title()) ==0)
			break;
	}
	if(i<magazines_.size() ){
		magazines_[i].More();
	}else{
		magazines_.push_back(Magazine(title,year,month,num));
	}

}	 
bool Library::DeleteBook(const string& title){// 도서 정보를 삭제하는 함수
	int i;					COUT<<"start DeleteBook("<<title<<")"<<endl;
	for(i=0; i<books_.size(); i++){
		if(title.compare(books_[i].title()) ==0)
			break;
	}
	if(i<books_.size() ){
		books_.erase(books_.begin()+i);
		return true;
	}
	for(i=0; i<magazines_.size(); i++){
		if(title.compare(magazines_[i].title()) ==0)
			break;
	}
	if(i<magazines_.size() ){
		magazines_.erase(magazines_.begin()+i);
		return true;
	}
	return false;
}

bool Library::Lend(const string& title){ // 책 한권을 대여하는 함수
	int i;				COUT<<"start lib::Lend("<<title<<")"<<endl;
	for(i=0; i<books_.size(); i++){
		if(title.compare(books_[i].title()) ==0)
			break;
	}
	if(i<books_.size() ){
		return books_[i].Lend();
	}
	for(i=0; i<magazines_.size(); i++){
		if(title.compare(magazines_[i].title()) )
			break;
	}
	if(i<magazines_.size() ){
		return magazines_[i].Lend();
	}
	return false;
}
bool Library::Return(const string& title){ // 책 한권을 반납하는 함수
	int i;				COUT<<"start lib::Return("<<title<<")"<<endl;
	for(i=0; i<books_.size(); i++){
		if(title.compare(books_[i].title()) ==0)
			break;
	}
	if(i<books_.size() ){
		return books_[i].Return();
	}
	for(i=0; i<magazines_.size(); i++){
		if(title.compare(magazines_[i].title()) ==0)
			break;
	}
	if(i<magazines_.size() ){
		return magazines_[i].Return();
	}
	return false;
}
void Library::PrintAll(){ // 현재 저장되어 있는 도서(책과 잡지)의 리스트를 출력. // 출력형식: stock/total title
	int i;
	for(i=0; i<books_.size(); i++){
		cout<<books_[i]<<endl;
	}
	
	for(i=0; i<magazines_.size(); i++){
		cout<<magazines_[i]<<endl;
	}
}


char itoc(int i){
	if(0<=i && i<=9) return i+'0';
	return 'X';
}

string str( int i){
	string new_str;
	int d=1;
	for(;i/(d*10)!=0; d*=10)
		continue;

	while(d!=0){
		new_str+=itoc(i/d);
		i%=d;
	d/=10;
	}
	return new_str;
}
