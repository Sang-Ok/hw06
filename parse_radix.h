//parse_radix.h
#ifndef _PARSE_RADIX_H_
#define _PARSE_RADIX_H_
#include <iostream>

class Number{
 public:
//Number():num(0),rad(10){}
  Number(int n=0, int r=10):num(n),rad(r){}
  Number(const Number& a){*this=a;}
  int num;
  int rad;
};

Number operator+(const Number& a, const Number& b);
Number operator-(const Number& a, const Number& b);
Number operator*(const Number& a, const Number& b);
Number operator/(const Number& a, const Number& b);

bool operator<(const Number& a, const Number& b);
bool operator>(const Number& a, const Number& b);
bool operator>=(const Number& a, const Number& b);
bool operator<=(const Number& a, const Number& b);
bool operator==(const Number& a, const Number& b);

Number&operator%(Number& a,const int& n);


std::istream& operator>>(std::istream& is, Number& pt);
std::ostream& operator<<(std::ostream& os, const Number& pt);

#endif
