#include "point2d.cc"
#include <map>
#include <string>
#include <cstdlib>
#define ERROR_MESSAGE__INVALID_COMMAND(command)	"There is no command like '"+command+"'. Please write down valid command as 'set', 'eval', 'quit'."
#define ERROR_MESSAGE__INVALID_NAME(name)	"input error"//"There is no name like '"+name+"'. During your settings."
#define ERROR_MESSAGE__INVALID_OPERATOR(op)	"There is no operater like '"+op+"' for points."

#define test cout<<"\t\tTEST::"

int strtoi(string str){
  for(int i=0; i<str.size(); i++)
    if(str[i]<'0'||'9'<str[i]) return 0xffff;
  return atoi(str.c_str());
}

bool NameToValue(const string&n, Point& v,const map<string,Point>&points){
  if(points.find(n)!=points.end()){
    v=points.find(n)->second;	////test<<v<<endl;
    return true;
  }else if( 0xffff != strtoi(n)){
    v=Point(strtoi(n));	////test<<v<<endl;
    return true;
  }else{ cout<<ERROR_MESSAGE__INVALID_NAME(n); return false;}
}

void DoOperation(const string& op, Point& v1,const Point v2){
  if(op=="+"){       cout<<(v1 + v2)<<endl;
  }else if(op=="-") {cout<<(v1 - v2)<<endl;
  }else if(op=="*") {cout<<(v1 * v2)<<endl;
  }else if(op=="+="){cout<<(v1 = v1 + v2)<<endl; //test<<v1<<endl;
  }else if(op=="-="){cout<<(v1 = v1 - v2)<<endl; //test<<v1<<endl;
  }else if(op=="*="){cout<<(v1 = v1 * v2)<<endl; //test<<v1<<endl;
  }else{ cout<<ERROR_MESSAGE__INVALID_OPERATOR(op);}
}

int main(){
  string command, name, n1, n2, op;
  Point  value, v1, v2;
  map<string,Point>points;
  while(true){//test<<"FIR v1=="<<v1<<endl;
    cin>>command;
    if(command=="set"){
      cin>>name>>value;
      points[name]=value;
    }else if(command=="eval"){
      cin>>n1>>op>>n2;
      if(!NameToValue(n1,v1,points))continue;	//test<<"v1=="<<v1<<endl;
      if(!NameToValue(n2,v2,points))continue;	//test<<"v2=="<<v2<<endl;
      DoOperation(op, v1, v2);			//test<<"v1=="<<v1<<endl;
      points.find(n1)->second=v1;
    }else if(command=="quit"){
      break;
    }else{cout<<ERROR_MESSAGE__INVALID_COMMAND(command);}//test<<"FIN v1=="<<v1<<endl;
  }
}
