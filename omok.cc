// omok.cc
#include "omok.h"

#define P(x,y) make_pair(x,y)


//static map<pair<int, int>, bool>ExtractEdge(bool checking_black, int way, const map<pair<int, int>& data_);
//static bool EdgeCheck(const int& way, const map<pair<int, int>& Edges);

static bool Check8(	   const pair<int,int>&p,const bool& checking_black, const map<pair<int,int>,bool>& data_);
static int  Check1(int way,const pair<int,int>&p,const bool& checking_black, const map<pair<int,int>,bool>& data_);



bool Omok::Put(int x, int y){
  if(x<0||19<=x  || y<0||19<=y)		return false;
  if(data_.find(P(x,y)) !=data_.end())  return false;
  data_[P(x,y)]=IsBlacksTurn_;
  IsBlacksTurn_ = !IsBlacksTurn_;
  return true;
}

bool Omok::IsOmok(bool* is_winner_black) const{
  map<pair<int, int>, bool>::const_iterator it;
  bool checking_black;
  int  way,count;

  for(count=0, checking_black=true; count<2; checking_black=!checking_black, count++){
    for(it=data_.begin(); it!=data_.end(); it++){
      if( Check8(*it,checking_black,data_) ){
        *is_winner_black=checking_black;
        return true;
      }
    }
  }
  return false;
}

int Check1(int way,const pair<int,int>&p,const bool& checking_black, const map<pair<int,int>,bool>& data_){
  if( data_.find(p)==data_.end())	return 0;
  if( data_.find(p)->second!=checking_black)	return 0;
  pair<int,int> new_p;
  new_p=(way==0)?	P(p.first+0 ,p.second+1)	:
	(way==1)?	P(p.first+1 ,p.second+1)	:
	(way==2)?	P(p.first+1 ,p.second+0)	:
	(way==3)?	P(p.first+1 ,p.second-1)	:
	(way==4)?	P(p.first+0 ,p.second-1)	:
	(way==5)?	P(p.first-1 ,p.second-1)	:
	(way==6)?	P(p.first-1 ,p.second+0)	:
	(way==7)?	P(p.first-1 ,p.second+1)	:
	/*ERROR CASE*/	P(-1,-1);
  return Check1(way,new_p,checking_black,data_)  +1;
}

bool Check8(const pair<int,int>&p,const bool& checking_black, const map<pair<int,int>,bool>& data_){
  int count=0;
  for(int way=0; way<4; way++)
    if(Check1(way  ,p,checking_black,data_) + Check1(way+4,p,checking_black,data_)-1  ==5)
      return true;
  return false;
}


std::ostream& operator<<(std::ostream& os, Omok& omok){
  string temp;
  int x,y;

  for(y=0; y<19 ; y++, temp=""){
    for(x=0; x<19 ; x++){
      if(omok.data().find(P(x,y))==omok.data().end())
        temp+='.';
      else
        temp+= ( omok.data()[P(x,y)] == true)? 'O':'x';
    }
  os<<temp<<endl;
  }
  return os;
}
























